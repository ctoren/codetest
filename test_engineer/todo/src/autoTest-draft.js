import React from 'react';
import Enzyme, {shallow} from 'enzyme';      //enzyme with jest 
import Adapter from 'enzyme-adapter-react-16';
import Todos from './Todo.jsx';

Enzyme.configure({adapter: new Adapter()});

it('Mark ToDo item checkbox on click', () => {
    //render checkbox
    const doneText = shallow(<TodoList markTodoDone=true />);

    expect(doneText.done()).toEqual(true);     //checkbox checked for done item 

    doneText.find('icon').simulate('click');

    expect(doneText.done()).toEqual(false);     //uncheck item that is undone 
});
    
    //expect(doneText.toBeTruthy();

it('Add ToDo item', () => {
    
    const addItem = shallow(<TodoList />);        //add item 

    expect(addItem.done()).toEqual(true);

    addItem.find('item').simulate('click');

    expect(addItem.done()).toEqual(false);
});
  
