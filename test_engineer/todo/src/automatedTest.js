//working with Lambda Test Selenium for auto test in JS


// ask the browser to open a page
//driver.navigate().to("http://localhost:3000/");

const webdriver = require("selenium-webdriver");
const driver = new webdriver.Builder().forBrowser("chrome").build();

describe('todo', function () {
    browser.ignoreSynchronization = true;
    it('New Test', function () {
        browser.get('http://localhost:3000/');  //navigate to localhost:3000
        browser.driver.findElement(by.className("icon")).then(function (foundElement){
            foundElement.click();   //click to check and uncheck the box of todo list item.
        });

        browser.driver.findElement(by.className('form-control')).then(function (foundElement) {
            foundElement.clear();
            foundElement.sendKeys("Pay Bills"); //when found element, add new item to the list "pay bills"
        });

        browser.driver.findElement(by.className('submit')).then(function (foundElement) {
            foundElement.click(); //click to submit the new item added to the list
        });

          browser.driver.findElement(by.className('close')).then(function (foundElement) {
            foundElement.click(); //click the element to close/get rid of the item from the list
        });

                 browser.driver.quit(); 

    });
});

  
  
  
